package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.api.repository.ICommandRepository;
import ru.tsc.bagrintsev.tm.constant.CommandLineConst;
import ru.tsc.bagrintsev.tm.constant.InteractionConst;
import ru.tsc.bagrintsev.tm.model.Command;

import static ru.tsc.bagrintsev.tm.constant.CommonConst.EMPTY;

public final class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(
            CommandLineConst.HELP_SHORT, InteractionConst.HELP, "Print help.");

    public static final Command ABOUT = new Command(
            CommandLineConst.ABOUT_SHORT, InteractionConst.ABOUT, "Print about.");

    public static final Command VERSION = new Command(
            CommandLineConst.VERSION_SHORT, InteractionConst.VERSION, "Print version.");

    public static final Command INFO = new Command(
            CommandLineConst.INFO_SHORT, InteractionConst.INFO, "Print system info.");

    public static final Command EXIT = new Command(
            EMPTY, InteractionConst.EXIT, "Close program.");

    public static final Command ARGUMENTS = new Command(
            CommandLineConst.ARGUMENTS_SHORT, InteractionConst.ARGUMENTS, "Print command-line arguments.");

    public static final Command COMMANDS = new Command(
            CommandLineConst.COMMANDS_SHORT, InteractionConst.COMMANDS, "Print application interaction commands.");

    public static final Command TASK_LIST = new Command(
            null, InteractionConst.TASK_LIST, "Print task list.");

    public static final Command TASK_CREATE = new Command(
            null, InteractionConst.TASK_CREATE, "Create new task.");

    public static final Command TASK_CLEAR = new Command(
            null, InteractionConst.TASK_CLEAR, "Remove all tasks.");

    public static final Command PROJECT_LIST = new Command(
            null, InteractionConst.PROJECT_LIST, "Print project list.");

    public static final Command PROJECT_CREATE = new Command(
            null, InteractionConst.PROJECT_CREATE, "Create new project.");

    public static final Command PROJECT_CLEAR = new Command(
            null, InteractionConst.PROJECT_CLEAR, "Remove all projects.");


    private final Command[] commands = new Command[]{
            HELP, ABOUT, VERSION, INFO,
            ARGUMENTS, COMMANDS,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            EXIT
    };

    @Override
    public Command[] getAvailableCommands() {
        return commands;
    }
}
