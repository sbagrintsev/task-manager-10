package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ru.tsc.bagrintsev.tm.api.repository.ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public void clear() {
        tasks.clear();
    }
}
