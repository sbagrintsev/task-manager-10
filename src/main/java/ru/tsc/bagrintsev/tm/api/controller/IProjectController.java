package ru.tsc.bagrintsev.tm.api.controller;

import java.io.IOException;

public interface IProjectController {

    void showProjectList();

    void clearProjects();

    void createProject() throws IOException;

}
