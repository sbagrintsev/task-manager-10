package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;

public interface IProjectService {

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    List<Project> findAll();

    void remove(Project project);

    void clear();

}
