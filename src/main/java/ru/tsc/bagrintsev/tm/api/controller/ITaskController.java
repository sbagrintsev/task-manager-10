package ru.tsc.bagrintsev.tm.api.controller;

import java.io.IOException;

public interface ITaskController {

    void showTaskList();

    void clearTasks();

    void createTask() throws IOException;

}
